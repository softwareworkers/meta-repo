# Software Workers meta-repository

Welcome to Software Workers global repository!

This is a git meta-repository, it **only** strictly contains organisation wide documentation, e.g.: how to contribute, internal code of conduct, general organisational rules, etc.

It also contains the `.mrconfig` configuration file used to manage the whole set of working trees via [myrepos](https://myrepos.branchable.com/).

# Use mr with this meta-repository 

## First usage

To use the `.mrconfig` in this meta-repository, simply clone its [upstream repo](https://gitlab.com/softwareworkers/meta-repo) using ssh:

```
git clone git@gitlab.com:softwareworkers/meta-repo.git $HOME/softwareworkers.intranet
```

As with all git local working directories, you can customise it by changing the ``$HOME/softwareworkers.intranet`` in the above command, or simply **renaming** or **moving** it to your preferred folder.

All you will get is a local working directory, e.g. `$HOME/softwareworkers.intranet` following the above example, where you can use the `mr` command from `myrepos` to manage all other repositories. 

## Getting content

All repositories defined in `.mrconfig` containing the ``skip = lazy`` configuration parameter are *skipped if the related working tree* is absent, usually this is the configuration for all repositories since a user can decide what to clone locally.

If you want to get a repository you have to explicitly clone it with:

```
  mr -fd $REPO_LABEL checkout
```

See `.mrconfig` for the complete list of repositories labels (the ones in square braces, e.g. `[infrastructure]`).

All mandatory repositories **should not** contain the `skip = lazy` myrepos directive, so will be automatically checked out when using `mr up`.

## Regular usage

When in the root of you local working directory, e.g. `$HOME/softwareworkers.intranet` in the above example, you can use `mr` to apply commands on all managed repositories, e.g.:

- `mr update` to update (pull in git parlance)
- `mr status` to have a report on the status of each local repository stus
- `mr commit` to commit staged changes
- `mr push` to push commits
- `mr diff` to have diffs

See `man mr` for a detailed manual, or [browse it online](https://manpages.debian.org/stretch/myrepos/mr.1.en.html)

### Chain loading

Chain loading means to combine several `.mrconfig` files into your *main* one, generally `$HOME/.mrrepo`.

To chain load this repository to your ``$HOME/.mrconfig``, add this configuration snippet:

```

 #################################################
 # SW meta repositories

 [${HOME}/softwareworkers.intranet]
 checkout = git clone 'git@gitlab.com:softwareworkers/meta-repo.git' $MR_REPO
 chain = true
 
```

For security reasons, you **have to** trust the newly added `.mrconfig` adding it in your `$HOME/.mrtrust`` file (create one if you haven't it):

```
  ~/softwareworkers.intranet/.mrconfig
```

# Adding files to this meta-repo

Since this is a meta-repository we **ignore all** working tree content via `.gitignore`: please *explicitly* add files you want to be tracked using:

```
  git add -f <file>
```

Mandatory files that **must always be tracked** are:

- .mrconfig
- .gitignore
